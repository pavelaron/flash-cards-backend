import express from 'express';
import * as db from './db.js';

const app = express();
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  respond(res, 'API status: Listening for requests');
});

app.get('/cards', (req, res) => {
  db.read((err, cards) => {
    respond(res, err || cards);
  });
});

app.post('/cards', (req, res) => {
  const body = checkParams(req, res);

  if (body) {
    db.create(body, (err, card) => {
      respond(res, err || card);
    });
  }
});

app.patch('/cards/:id', (req, res) => {
  const body = checkParams(req, res);

  if (body) {
    const card = {
      id: req.params.id,
      question: body.question,
      answer: body.answer
    };

    db.update(card, (err, card) => {
      respond(res, err || card);
    });
  }
});

const checkParams = (req, res) => {
  if (!req.body.answer) {
    respond(res, 'Bad request', 400);
    return false;
  }

  return req.body;
};

const respond = (res, message, statusCode) => {
  const responseObject = {
    status: statusCode || 200,
    response: message
  };

  res.setHeader('Content-Type', 'application/json');
  res.send(responseObject);
};

db.connect((err) => {
  if (err) throw err;

  const server = app.listen(process.env.PORT || 3000, () => {
    const host = server.address().address;
    const port = server.address().port;
     
    console.log(`Flash card app listening at http://${host}:${port}`);
  });
});

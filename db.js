import { MongoClient, ObjectID } from 'mongodb';

const url = 'mongodb://localhost:27017/card-db';
const collectionName = 'cards';

let db;

const connect = (callback) => {
  MongoClient.connect(url, (err, database) => {
    if (err) return callback(err);

    db = database.db('card-db');
    db.createCollection(collectionName, { 
      strict: true 
    }, (error, collection) => {
      callback();
    });
  });
};

const create = (card, callback) => {
  db.collection(collectionName).insert({
    question: card.question,
    answer: card.answer
  }, (err, result) => {
    callback(err, result);
  });
};

const read = (callback) => {
  db.collection(collectionName).find().toArray((err, result) => {
    callback(err, result);
  });
};

const update = (card, callback) => {
  db.collection(collectionName).update({ '_id': ObjectID(card.id) }, {
    $set: {
      question: card.question,
      answer: card.answer
    }
  }, (err, result) => {
    callback(err, result);
  });
};

export { connect, create, read, update }
